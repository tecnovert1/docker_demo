
git clone git@gitlab.com:tecnovert1/docker_demo.git

sudo docker network create nginx-proxy

sudo docker-compose -f ~/docker_demo/letsencrypt/docker-compose.yml up -d

sudo docker-compose -f ~/docker_demo/site1/docker-compose.yml up -d
sudo docker-compose -f ~/docker_demo/site2/docker-compose.yml up -d

sudo docker-compose -f ~/docker_demo/letsencrypt/docker-compose.yml logs -f
